package com.yuls.nerplayer.nerplayer;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

public class PlayActivity extends AppCompatActivity implements View.OnClickListener{

    String TAG = "nerplayer";
    TextView tvShow;
    Button btnPlay, btnPrev, btnNext;
    SeekBar sbProgress;
    String name;
    String path;
    boolean bound;

    public static final int MSG_RCV_PROGRESS = 1;
    public static final int MSG_SND_PROGRESS = 2;
    public static final int MSG_SND_PLAY = 3;
    public static final int MSG_SND_PAUSE = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);
        name = getIntent().getStringExtra("name");
        path = getIntent().getStringExtra("path");
        initViews();
    }

    @Override
    protected void onStart() {
        super.onStart();
        doBound();
    }

    @Override
    protected void onStop() {
        super.onStop();
        doUnBound();
    }

    ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            serviceMsgr = new Messenger(service);
            try {
                Message msg = Message.obtain(null, PlayService.MSG_REGISTER_CLIENT);
                msg.replyTo = inMsgr;
                serviceMsgr.send(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            try {
                Message msg = Message.obtain(null, PlayService.MSG_UNREGISTER_CLIENT);
                msg.replyTo = inMsgr;
                serviceMsgr.send(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            serviceMsgr = null;
        }
    };


    // UIThread,同时接收service消息
    Handler mH = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            Message sndMsg;
            switch (msg.what) {
                case MSG_RCV_PROGRESS:
                    sbProgress.setProgress(msg.arg1);
                    break;
                case MSG_SND_PLAY:
                    sndMsg = Message.obtain(null, PlayService.MSG_PLAY);
                    sndMsg.obj = new Music(PlayActivity.this.name, path);
                    try {
                        serviceMsgr.send(sndMsg);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                    break;
                case MSG_SND_PAUSE:
                    sndMsg = Message.obtain(null, PlayService.MSG_PAUSE);
                    sndMsg.obj = new Music(PlayActivity.this.name, path);
                    try {
                        serviceMsgr.send(sndMsg);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                    break;
                case MSG_SND_PROGRESS:
                    sndMsg = Message.obtain(null, PlayService.MSG_SET_PROGRESS);
                    sndMsg.arg1 = msg.arg1;
                    sndMsg.obj = new Music(PlayActivity.this.name, path);
                    try {
                        serviceMsgr.send(sndMsg);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    };

    // 接收service消息
    Messenger inMsgr = new Messenger(mH);
    // 给service发送消息
    Messenger serviceMsgr;



    private void doBound() {
        if (!bound) {
            bound = true;
            Intent i = new Intent(this, PlayService.class);
            i.putExtra("name", name);
            i.putExtra("path", path);
            bindService(i, conn, BIND_AUTO_CREATE);
            isPlaying = true;
        }
    }

    private void doUnBound() {
        if (bound) {
            bound = false;
            unbindService(conn);
        }
    }

    private void initViews() {
        tvShow = findViewById(R.id.tv_show);
        tvShow.setText("play:" + name);
        sbProgress = findViewById(R.id.sb_progress);
        sbProgress.setMax(100);
        sbProgress.setOnSeekBarChangeListener(mSeekBarListener);
        btnPlay = findViewById(R.id.btn_play);
        btnPlay.setOnClickListener(this);
        btnPrev = findViewById(R.id.btn_prev);
        btnPrev.setOnClickListener(this);
        btnNext = findViewById(R.id.btn_next);
        btnNext.setOnClickListener(this);
    }

    private SeekBar.OnSeekBarChangeListener mSeekBarListener = new SeekBar.OnSeekBarChangeListener(){
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            Log.i(TAG, "onProgressChanged, progress: " + progress + ", fromUser: " + fromUser);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            Log.i(TAG, "onStartTrackingTouch, progress:" + seekBar.getProgress());
            mH.sendEmptyMessage(MSG_SND_PAUSE);
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            Log.i(TAG, "onStopTrackingTouch, progress:" + seekBar.getProgress());
            Message msg = mH.obtainMessage(MSG_SND_PROGRESS, seekBar.getProgress(), -1);
            mH.sendMessage(msg);
            mH.sendEmptyMessageDelayed(MSG_SND_PLAY, 200L);
        }
    };

    boolean isPlaying;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_play:
                try {
                    Message msg;
                    if (isPlaying) {
                        msg = Message.obtain(null, PlayService.MSG_PAUSE);
                        btnPlay.setText("play");
                        isPlaying = false;
                    } else {
                        msg = Message.obtain(null, PlayService.MSG_PLAY);
                        btnPlay.setText("pause");
                        isPlaying = true;
                    }
                    msg.obj = new Music(PlayActivity.this.name, path);
                    serviceMsgr.send(msg);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.btn_prev:
                break;
            case R.id.btn_next:
                break;
        }
    }
}
