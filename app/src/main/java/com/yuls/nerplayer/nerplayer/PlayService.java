package com.yuls.nerplayer.nerplayer;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PlayService extends Service {

    String TAG = "nerplayer";
    public static final int MSG_REGISTER_CLIENT = 1;
    public static final int MSG_UNREGISTER_CLIENT = 2;
    public static final int MSG_PLAY = 3;
    public static final int MSG_PAUSE = 4;
    public static final int MSG_PREV = 5;
    public static final int MSG_NEXT = 6;
    public static final int MSG_PROGRESS = 7;
    public static final int MSG_SET_PROGRESS = 8;
    public static final int MSG_QUIT = 9;
    private List<Messenger> mClientMsgrs;
    String name;
    String path;
    boolean bounded;
    
    public PlayService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mClientMsgrs = new ArrayList<>();
    }

    @Override
    public IBinder onBind(Intent intent) {
        name = intent.getStringExtra("name");
        path = intent.getStringExtra("path");
        bounded = true;
        new Thread(new PlayRunnable()).start();
        return msgr.getBinder();
    }


    // 接受Activity消息
    class InCommingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_REGISTER_CLIENT:
                    mClientMsgrs.add(msg.replyTo);
                    break;
                case MSG_UNREGISTER_CLIENT:
                    Messenger client = msg.replyTo;
                    if (client != null && mClientMsgrs.contains(client)) {
                        Log.i(TAG, "remove client:" + client);
                        mClientMsgrs.remove(client);
                    } else {
                        Log.e(TAG, "client is null:" + (client == null) +
                                ", is contain:" + mClientMsgrs.contains(client));
                    }
                    break;
                case MSG_PLAY:
                    playHandler.sendEmptyMessage(MSG_PLAY);
                    break;
                case MSG_PAUSE:
                    playHandler.sendEmptyMessage(MSG_PAUSE);
                    break;
                case MSG_SET_PROGRESS:
                    Message sndMsg = Message.obtain(null, MSG_SET_PROGRESS, msg.arg1, -1);
                    playHandler.sendMessage(sndMsg);
                    break;
            }
        }
    }

    Messenger msgr = new Messenger(new InCommingHandler());

    private Handler playHandler;

    class PlayRunnable implements Runnable {

        public PlayRunnable() {
            mp = new MediaPlayer();
            try {
                mp.setDataSource(path);
                mp.prepare();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        MediaPlayer mp;

        @Override
        public void run() {
            mp.start();
            Looper.prepare();
            playHandler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    switch (msg.what) {
                        case MSG_PLAY:
                            if (!mp.isPlaying()) {
                                Log.i(TAG, "MediaPlayer play, position:" + mp.getCurrentPosition());
                                mp.start();
                            }
                            break;
                        case MSG_PAUSE:
                            if (mp.isPlaying()) {
                                Log.i(TAG, "MediaPlayer pause, position:" + mp.getCurrentPosition());
                                mp.pause();
                                playHandler.removeMessages(MSG_PROGRESS);
                            }
                            break;
                        case MSG_PROGRESS:
                            if (bounded && mp.isPlaying()) {
                                // Log.i(TAG, "progress: " + (100 * mp.getCurrentPosition() / mp.getDuration()) + "%");
                                for (Messenger clientMsgr : mClientMsgrs) {
                                    Message progressMsg = Message.obtain(null, PlayActivity.MSG_RCV_PROGRESS);
                                    try {
                                        progressMsg.arg1 = 100 * mp.getCurrentPosition() / mp.getDuration();
                                        clientMsgr.send(progressMsg);
                                    } catch (RemoteException e) {
                                        e.printStackTrace();
                                    }
                                }
                                playHandler.sendEmptyMessageDelayed(MSG_PROGRESS, 200L);
                            }
                            break;
                        case MSG_SET_PROGRESS:
                            if (bounded) {
                                int msec = mp.getDuration() * msg.arg1 / 100;
                                Log.i(TAG, "MediaPlayer seekTo: " + msec + "ms (" + msg.arg1 + "%)");
                                mp.seekTo(msec);
                            }
                            break;
                        case MSG_QUIT:
                            Looper.myLooper().quit();
                            break;
                    }
                }
            };
            playHandler.sendEmptyMessageDelayed(MSG_PROGRESS, 1000L);
            Looper.loop();
            //TODO 后面的代码不执行
        }
    }

    @Override
    public boolean onUnbind(Intent intent) {
        bounded = false;
        playHandler.sendEmptyMessage(MSG_PAUSE);
        playHandler.sendEmptyMessage(MSG_QUIT);
        return super.onUnbind(intent);
    }
}
