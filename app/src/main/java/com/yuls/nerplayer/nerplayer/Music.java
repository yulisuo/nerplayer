package com.yuls.nerplayer.nerplayer;

import java.io.File;

public class Music {
    String name;
    String path;

    public Music(String name, String path) {
        this.name = name;
        this.path = path;
    }

    public Music(File f) {
        String fileName = f.getName();
        // hard code 修剪文件名
        name = fileName.substring(0, fileName.indexOf("[mqms2"));
        path = f.getAbsolutePath();
    }

    @Override
    public String toString() {
        return "Music{" +
                "name='" + name + '\'' +
                ", path='" + path + '\'' +
                '}';
    }
}
