package com.yuls.nerplayer.nerplayer;

import android.Manifest;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{

    String TAG = "nerplayer";
    private MyHandler mH;
    private static final int REQUEST_CODE = 1;
    private static final int MSG_SHOW_LIST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mH = new MyHandler();
        if (PackageManager.PERMISSION_GRANTED !=
                checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE);
        } else {
            doScan();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            doScan();
        }
    }

    private void doScan() {
        Log.i(TAG, "doScan");
        new Thread(scanRunnable).start();
    }


    private Runnable scanRunnable = new Runnable() {
        @Override
        public void run() {
            String path = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "Music";
            File musicFolder = new File(path);
            ArrayList<Music> ms = new ArrayList<>();
            if (musicFolder.exists()) {
                File[] files = musicFolder.listFiles();
                if (files != null && files.length > 0) {
                    for (File f : files) {
                        if (f.isFile() && f.getName().endsWith(".mp3")) {
                            Music music = new Music(f);
                            ms.add(music);
                        }
                    }
                }
            }
            if (ms.size() > 0) {
                mH.sendMessage(mH.obtainMessage(MSG_SHOW_LIST, ms));
            }
        }
    };

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Music m = ms.get(position);
        Log.i(TAG, "click item:" + m);
        Intent i = new Intent(this, PlayActivity.class);
        i.putExtra("name", m.name);
        i.putExtra("path", m.path);
        startActivity(i);
    }

    ArrayList<Music> ms;

    class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_SHOW_LIST:
                    ListView lv = findViewById(R.id.list_music);
                    ms = (ArrayList<Music>) msg.obj;
                    List<Map<String, String>> data = new ArrayList<>();
                    for (Music m : ms) {
                        Map<String, String> map = new HashMap<>();
                        map.put("name", m.name);
                        data.add(map);
                    }
                    lv.setAdapter(new SimpleAdapter(MainActivity.this, data,
                            R.layout.music_item, new String[]{"name"}, new int[]{R.id.tv_music_name}));
                    lv.setOnItemClickListener(MainActivity.this);
                    break;
            }
        }
    }
}
